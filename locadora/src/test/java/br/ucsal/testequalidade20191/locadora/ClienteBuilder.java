package br.ucsal.testequalidade20191.locadora;

import br.ucsal.testequalidade20191.locadora.dominio.Cliente;

public class ClienteBuilder {

	private static final String cpfDF = "111111111";

	private static final String nomeDF = "Luiz";

	private static final String telefoneDF = "9551515555";

	private String cpf = cpfDF;

	private String nome = nomeDF;

	private String telefone = telefoneDF;

	public  ClienteBuilder() {

	}

	public static ClienteBuilder umCliente() {
		return new ClienteBuilder(); 

	}

	public  ClienteBuilder comCpf(String cpf) {
		this.cpf = cpf;
		return this;

	}

	public ClienteBuilder comNome(String nome) {
		this.nome = nome;
		return this;

	}

	public ClienteBuilder comTelefone(String telefone) {
		this.telefone = telefone;
		return this;

	}

	public ClienteBuilder mas() {
		return umCliente().comCpf(cpf).comNome(nome).comTelefone(telefone);

	}

	public Cliente builde() {
		Cliente cliente = new Cliente(cpf, nome, telefone);
		cliente.setCpf(cpf);
		cliente.setNome(nome);
		cliente.setTelefone(telefone);
		return cliente;

	}

}
