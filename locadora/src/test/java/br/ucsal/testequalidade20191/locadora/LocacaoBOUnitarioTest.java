package br.ucsal.testequalidade20191.locadora;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.ucsal.testequalidade20191.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20191.locadora.dominio.Cliente;
import br.ucsal.testequalidade20191.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20191.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20191.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20191.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20191.locadora.persistence.VeiculoDAO;

@RunWith(MockitoJUnitRunner.class)
public class LocacaoBOUnitarioTest {

	@Mock
	private ClienteDAO clienteDAOMok;
	@Mock
	private VeiculoDAO veiculoDAOMok;
	@Mock
	private LocacaoDAO locacaoDAOMok;
	@InjectMocks
	private LocacaoBO locacaoBO;

	/**
	 * Locar, para um cliente cadastrado, um veículo disponível. Método:
	 * public static Integer locarVeiculos(String cpfCliente, List<String>
	 * placas, Date dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado {
	 *
	 * Observação: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * @throws Exception
	 */
	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		String cpf = "222222";
		Cliente cliente = ClienteBuilder.umCliente().comCpf(cpf).builde();
		Mockito.doReturn(cliente).when(clienteDAOMok).obterPorCpf(cpf);
		SituacaoVeiculoEnum situacao = SituacaoVeiculoEnum.DISPONIVEL;
		String placa = "1111";
		Veiculo veiculo = VeiculoBuilder.umVeiculo().comPlaca(placa).comSituacaoDisponivel(situacao).Builder();
		Mockito.doReturn(veiculo).when(veiculoDAOMok).obterPorPlaca(placa);

		List<String> placas = Arrays.asList("1122");
		Date dataLocacao = null;
		Integer quantidadeDiasLocacao = null;


		/*
		 * Integer actual = locacaoBO.locarVeiculos(cpf, placas, dataLocacao,
		 * quantidadeDiasLocacao);
		 * 
		 * Integer expected = null; Assert.assertEquals(expected, actual);
		 * Mockito.verify(clienteDAOMok).insert(cliente);
		 * Mockito.verify(veiculoDAOMok).insert(veiculo);
		 */
		Mockito.verify(clienteDAOMok).obterPorCpf(cpf);
		Mockito.verify(veiculoDAOMok).obterPorPlaca(placa);
	}
}
