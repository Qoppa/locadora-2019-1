package br.ucsal.testequalidade20191.locadora.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.ucsal.testequalidade20191.locadora.dominio.Cliente;
import br.ucsal.testequalidade20191.locadora.dominio.Locacao;
import br.ucsal.testequalidade20191.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20191.locadora.dominio.enums.SituacaoVeiculoEnum;
import br.ucsal.testequalidade20191.locadora.exception.CampoObrigatorioNaoInformado;
import br.ucsal.testequalidade20191.locadora.exception.ClienteNaoEncontradoException;
import br.ucsal.testequalidade20191.locadora.exception.VeiculoNaoDisponivelException;
import br.ucsal.testequalidade20191.locadora.exception.VeiculoNaoEncontradoException;
import br.ucsal.testequalidade20191.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20191.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20191.locadora.persistence.VeiculoDAO;

public class LocacaoBO {

	private ClienteDAO clienteDAO;
	private VeiculoDAO veiculoDAO;
	private LocacaoDAO locacaoDAO;

	public LocacaoBO(ClienteDAO clienteDAO, VeiculoDAO veiculoDAO, LocacaoDAO locacaoDAO) {
		super();
		this.clienteDAO = clienteDAO;
		this.veiculoDAO = veiculoDAO;
		this.locacaoDAO = locacaoDAO;
	}

	/**
	 * Verifica se as condições de locação são atendidas: 1. Cliente deve
	 * estar previamente cadastrado 2. O(s) veículo(s) selecionado(s) deve(m)
	 * estar cadastrados 3. O(s) veículo(s) selecionado(s) deve(m) estar
	 * disponíveis 4. Devem ser informados: data de locação e quantidade dias
	 * de locação 5. Ao menos um veículo deve ter sido selecionado
	 * 
	 * Caso as condições tenham sido atendidas, este método solicitará o
	 * cadastramento do contrato de locação e retornará o número do contrato
	 * gerado.
	 * 
	 * O não atendimento das condições levantará uma exceção.
	 * 
	 * @param cpfCliente
	 *            - CPF do cliente que está contratando a locação do
	 *            veículo(s)
	 * @param placas
	 *            - placa(s) do(s) veículo(s) que serão locados
	 * @param dataLocacao
	 *            - data de início da locação
	 * @param quantidadeDiasLocacao
	 *            - quantidade de dias de locação
	 * @return número do contrato de locação
	 * @throws ClienteNaoEncontradoException
	 *             - cliente não cadastrado
	 * @throws VeiculoNaoEncontradoException
	 *             - pelo menos um dos veículos selecionados para locação
	 *             não está cadastrado
	 * @throws VeiculoNaoDisponivelException
	 *             - pelo menos um dos veículos selecionados para locação
	 *             não está disponível
	 * @throws CampoObrigatorioNaoInformado
	 *             - pelo menos um dos campo obrigatório não foi informado
	 */

	public void locarVeiculos(String cpfCliente, List<String> placas, Date dataLocacao, Integer quantidadeDiasLocacao)
			throws ClienteNaoEncontradoException, VeiculoNaoEncontradoException, VeiculoNaoDisponivelException,
			CampoObrigatorioNaoInformado {

		Cliente cliente = clienteDAO.obterPorCpf(cpfCliente);

		List<Veiculo> veiculos = new ArrayList<>();
		for (String placa : placas) {
			Veiculo veiculo = veiculoDAO.obterPorPlaca(placa);
			if (!SituacaoVeiculoEnum.DISPONIVEL.equals(veiculo.getSituacao())) {
				throw new VeiculoNaoDisponivelException();
			}
			veiculos.add(veiculo);
		}

		Locacao locacao = new Locacao(cliente, veiculos, dataLocacao, quantidadeDiasLocacao);

		locacaoDAO.insert(locacao);
	}

}
